<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * IconSelector.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\FormWidgets;

use Backend\Classes\FormWidgetBase;
use PlanetaDelEste\Features\Models\Icon;

/**
 * IconSelector Form Widget
 */
class IconSelector extends FormWidgetBase
{

    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'iconselector';

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->bindToController();
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('iconselector');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['field'] = $this->formField;
        $this->vars['name']  = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
        $this->vars['icons'] = Icon::all();
    }

    /**
     * {@inheritDoc}
     */
    public function loadAssets()
    {
        $this->addCss('css/tiles.css', 'PlanetaDelEste.Features');
        $this->addCss('css/iconselector.css', 'PlanetaDelEste.Features');
        $this->addJs('js/iconselector.js', 'PlanetaDelEste.Features');
    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        return $value;
    }

    public function onLoadIcons() {
        $this->prepareVars();
        return $this->makePartial('iconselector_form');
    }

}
