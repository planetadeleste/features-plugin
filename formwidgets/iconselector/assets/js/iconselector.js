/*
 * Copyright (c) 2016 Planeta del Este .
 *
 * iconselector.js is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

+function ($) { "use strict";

    var IconSelector = function (element, options) {
        var $el = this.$el = $(element);
        this.options = options || {};

        $el.on('click', this.selectIcon);

    };

    IconSelector.prototype.constructor = IconSelector;
    IconSelector.prototype.selectIcon = function() {
        var options = $(this).data('oc.iconSelector').options;
        if(options.iconField && options.iconValue) {
            var $field = $(options.iconField);
            if($field.length) {
                $field.val( options.iconValue );
            }
            if (options.iconContainer) {
                $(options.iconContainer + ' .icn-container > i').removeClass().addClass(options.iconValue + ' fa-4x');
                $(options.iconContainer + ' .icn-value').text(options.iconValue);
            }
        }

        if(options.iconPopup) {
            var $modal = $(options.iconPopup);
            if ($modal.length) {
                $modal.parents(".control-popup:first").modal('hide');
            }
        }
        return false;
    };

    IconSelector.DEFAULTS = {
        iconField : null,
        iconValue : null,
        iconContainer: null,
        iconPopup: null
    };

    // TOOLBAR PLUGIN DEFINITION
    // ============================

    var old = $.fn.iconSelector;

    $.fn.iconSelector = function (option) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function () {
            var $this = $(this);
            var data  = $this.data('oc.iconSelector');
            var options = $.extend({}, IconSelector.DEFAULTS, $this.data(), typeof option == 'object' && option);

            if (!data) $this.data('oc.iconSelector', (data = new IconSelector(this, options)));
            if (typeof option == 'string') data[option].apply(data, args)
        })
    };

    $.fn.iconSelector.Constructor = IconSelector;

    // TOOLBAR NO CONFLICT
    // =================

    $.fn.iconSelector.noConflict = function () {
        $.fn.iconSelector = old;
        return this
    };

    // TOOLBAR DATA-API
    // ===============

    $(document).on('render', function(){
        $('[data-control=iconselector]').iconSelector()
    })

}(window.jQuery);