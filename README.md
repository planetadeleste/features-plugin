# OCtoberCMS Features plugin #

![Dashboard](http://octobercms.com/storage/app/uploads/public/567/27d/297/56727d2974a82878152406.png)

Features plugin make it possible to have an unlimited of attributes, features, amenities or how your plugin must be name it.

It's very usable and flexible, can be adapted to your own plugin.

## Specs
##### Categories
Organize the features by one level categories

##### Font Icons libraries
Import from Url or upload your own font icons library.
Can make your own in flaticons.com, upload and use it in features.

##### Set features
*   Features can require value or not. 
*   Manually sort order
*   Set hidden (for private purpose)
*   Set to show in frontend search form (used in a Real Estate search form)

## Usage examples
##### Real Estate
A real estate website have properties (houses), each one must need to define amenities. Like; baths numbers, bedrooms, if have pool, etc.

##### Rent a car
Each vehicle of the website need to specify their properties. Such as color; number of seats; amount of airbags; Audio system; etc.

##### Fashion store
In the sale of clothing you need to specify the size, color, measures, etc.

## Showcase
*   Real Estate - [www.byr.com.uy](http://www.byr.com.uy/propiedades)
*   Rent a car - [www.josegarrido.com](http://www.josegarrido.com/rent-a-car)

## TODO
*	Fix sort order in relationship manage list
*	Compatibility with RainLab Translate plugin
*	Ability to have option select. Example: used in clothes shop, can select sizes like: S, M, L, XL

## Installation
Install from marketplace or from backend panel, using `PlanetaDelEste.Features`.

This plugin use [`sabberworm/php-css-parser`](https://github.com/sabberworm/PHP-CSS-Parser) and [`chumper/zipper`](https://github.com/Chumper/Zipper) packages.

## Make relationship
For this example, your plugin name is `Acme\Foo` and the model name is `Entry`

##### Create table required for many to many relation

	namespace Acme\Foo\Updates;
	
	use Schema;
	use October\Rain\Database\Updates\Migration;
	
	class CreateEntriesTable extends Migration
	{
	
	    public function up()
	    {
	        Schema::create('acme_foo_entries', function($table)
	        {
	            $table->engine = 'InnoDB';
	            $table->increments('id');
	            $table->timestamps();
	            $table->string('title');
	            $table->string('slug');
	        });
	
	        Schema::create('acme_foo_entries_features', function ($table)
	        {
	            $table->engine = 'InnoDB';
	            $table->integer('feature_id')->unsigned();
	            $table->integer('entry_id')->unsigned();
	            $table->boolean('is_public')->default(true);
	            $table->string('value', 200)->nullable();
	            $table->text('info')->nullable();
	
	            $table->primary(['entry_id', 'feature_id'], 'entries_features');
	        });
	    }
	
	    public function down()
	    {
	        Schema::dropIfExists('acme_foo_entries');
	        Schema::dropIfExists('acme_foo_entries_features');
	    }
	
	}


##### Extend Features plugin adding relationship
From your `Plugin.php` file, write this in `boot` method. 

	use PlanetaDelEste\Features\Models\Feature as FeatureModel;
	class Plugin extends PluginBase
	{
	    public function boot()
	    {
	        // Features
	        FeatureModel::extend(
	            function (FeatureModel $model) {
	                $model->hasMany['entries'] = [
	                    '\PlanetaDelEste\Directory\Models\Entry',
	                    'table'    => 'acme_foo_entries_features',
	                    'key'      => 'feature_id',
	                    'otherKey' => 'entry_id',
	                ];
	            }
	        );
	    }
	}

##### Model `Acme\Foo\Models\Entry`


	namespace Acme\Foo\Models
	
	use Model;
	class Entry extends Model
	{
	    /**
	     * @var string The database table used by the model.
	     */
	    public $table = 'acme_foo_entries';
	    
	    public $belongsToMany = [
	        'features'     => [
	            'PlanetaDelEste\Features\Models\Feature',
	            'table'    => 'acme_foo_entries_features',
	            'key'      => 'entry_id',
	            'otherKey' => 'feature_id',
	            'pivot'    => ['value', 'info'],
	            'order'    => 'planetadeleste_features_features.sort_order asc'
	        ],
	    ];	
	}



##### Controller `Acme\Foo\Controllers\Entries`

	namespace Acme\Foo\Controllers
	
	use Backend\Classes\Controller;
	class Entries extends Controller
	{
	    public $implement = [
	        'Backend.Behaviors.FormController',
	        'Backend.Behaviors.ListController',
	        'Backend.Behaviors.RelationController',
	        'PlanetaDelEste.Features.Behaviors.IconsController',
	    ];	
	    
	    public $formConfig = 'config_form.yaml';
	    public $listConfig = 'config_list.yaml';
	    public $relationConfig = 'config_relation.yaml';
	}

##### File `config_relation.yaml`

	# ===================================
	#  Relation Behavior Config
	# ===================================
	features:
	    label: Features
	    manage:
	        list: $/planetadeleste/features/models/feature/columns.yaml
	        form: $/planetadeleste/features/models/feature/fields.yaml
	        showSearch: true
	        defaultSort:
	            column: planetadeleste_features_features.sort_order
	            direction: asc
	    pivot:
	        form: $/acme/foo/models/entry/features_fields.yaml
	    view:
	        list: $/acme/foo/models/entry/features_columns.yaml
	        toolbarButtons: add|remove
	        showSorting: false
	        defaultSort:
	            column: planetadeleste_features_features.sort_order
	            direction: asc

##### File `acme/foo/models/entry/features_fields.yaml`

	fields:
	    pivot[value]:
	        label: Value
	        span: left
	    pivot[info]:
	        label: Info
	        span: right

##### File `acme/foo/models/entry/features_columns.yaml`

	columns:
	    name:
	        label: Name
	        sortable: false
	    category:
	        label: Category
	        relation: category
	        select: name
	        sortable: false
	    pivot[value]:
	        label: Value
	        sortable: false

## Loading icons libraries
There are two ways to load font icons stylesheets

#### Using controller behavior (for backend controller)

	public $implement = ['PlanetaDelEste.Features.Behaviors.IconsController',];

#### From icon model

	// $controller must be an instance of Cms or Backend Controller class
	Icon::loadAssets($controller);

# Icon
Manage icons libraries to install and organize your font icons, used in features items.

#### Create library from Url
![Create Icon](https://box.everhelper.me/attachment/352954/r7lzpnvLFnkALhTZaxr2E4PptDrX4Yrf/397815-oTLNylFzDifcIT0T/screen.png)

#### Create library from package
![Create Library from package](https://box.everhelper.me/attachment/352956/r7lzpnvLFnkALhTZaxr2E4PptDrX4Yrf/397815-qfpoqr0MLbBPDbQH/screen.png)

After library is created, three new tabs are shown.

#### Icons
From here, uncheck non icon class checkboxes. Must need save and refresh to view the changes.

#### View Icons
View icons in library, in a grid layout. From here you can see what css class is for icon and uncheck the others from previous tab.

#### Editor
From here you can edit the css stylesheet. Be careful, make changes at your own risk.