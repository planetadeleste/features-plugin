<?php namespace PlanetaDelEste\Features\Components;

use Cms\Classes\ComponentBase;
use PlanetaDelEste\Features\Models\Icon;

class Icons extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => trans('planetadeleste.features::lang.components.icons.name'),
            'description' => trans('planetadeleste.features::lang.components.icons.description')
        ];
    }

    public function defineProperties()
    {
        return [
            'css' => [
                'title'       => trans('planetadeleste.features::lang.components.icons.css_title'),
                'description' => trans('planetadeleste.features::lang.components.icons.css_description'),
                'type'        => 'dropdown',
                'default'     => 'all'
            ]
        ];
    }

    public function getCssOptions()
    {
        $options = ['all' => trans('planetadeleste.features::lang.components.icons.css_all')];

        $icons = Icon::all()->lists('library', 'id');
        if(count($icons)) {
            $options = array_merge($options, $icons);
        }

        return $options;
    }

    public function onRun()
    {
        if($this->property('css') == 'all') {
            Icon::loadAssets($this->controller);
        } else {
            /** @var Icon $icon */
            $icon = Icon::find($this->property('css'));
            if($icon) {
                $this->addCss($icon->assets_url);
            }
        }
    }
}
