<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * Features.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use PlanetaDelEste\Features\Models\Feature;
use PlanetaDelEste\Features\Models\Icon;

/**
 * Features Back-end Controller
 */
class Features extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'Backend.Behaviors.ReorderController',
        'PlanetaDelEste.Features.Behaviors.ListDelete',
        'PlanetaDelEste.Features.Behaviors.IconsController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['planetadeleste.features.access_features'];
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PlanetaDelEste.Features', 'features', 'features');
        $this->addCss('/plugins/planetadeleste/features/assets/css/planetadeleste-features-backend.css');

    }

}