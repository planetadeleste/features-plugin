<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * Category.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Models;

use Model;

/**
 * Category Model
 */
class Category extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_features_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name'];

    public $rules = ['name' => 'required'];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'features' => ['PlanetaDelEste\Features\Models\Feature']
    ];

}