<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * IconsController.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * PlanetaDelEste.Features
 * Created by alvaro.
 * User: alvaro
 * Date: 15/12/15
 * Time: 09:10 AM
 */

namespace PlanetaDelEste\Features\Behaviors;

use Backend\Classes\ControllerBehavior;
use PlanetaDelEste\Features\Models\Icon;

class IconsController extends ControllerBehavior
{
    /**
     * Behavior IconsController constructor.
     *
     * @param \Backend\Classes\Controller $controller
     */
    public function __construct($controller)
    {
        set_time_limit(0);
        parent::__construct($controller);

        if(method_exists($this->controller, 'overrideAddAssets')){
            $this->controller->overrideAddAssets();
        } else {
            $this->addAssets();
        }
    }

    public function addAssets()
    {
        Icon::loadAssets($this->controller);
    }
}