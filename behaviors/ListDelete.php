<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * ListDelete.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Behaviors;

use Backend\Classes\ControllerBehavior;
use Flash;
use Lang;

class ListDelete extends ControllerBehavior
{

    /**
     * @var \Backend\Classes\Controller
     */
    protected $controller;

    /**
     * Behavior constructor
     *
     * @param   \Backend\Classes\Controller $controller
     */
    public function __construct($controller)
    {
        parent::__construct($controller);

        $this->controller = $controller;

        $this->setConfig($controller->listConfig, ['modelClass']);
    }

    /**
     * Delete the selected rows
     *
     * @return  array
     */
    public function index_onDelete()
    {
        $model = $this->config->modelClass;

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            $error = false;
            foreach ($checkedIds as $id) {
                if ($record = $model::find($id)) {
                    if (method_exists($this->controller, 'overrideListDelete')) {
                        $this->controller->overrideListDelete($record);
                    } else {
                        if (!$record->delete()) {
                            $error = true;
                        }
                    }
                }
            }

            if (method_exists($this->controller, 'afterListDelete')) {
                $this->controller->afterListDelete($error);
            } elseif (!$error) {
                Flash::success(Lang::get('backend::lang.list.delete_selected_success'));
            }
        }

        return method_exists($this->controller, 'overrideListRefresh')
            ? $this->controller->overrideListRefresh()
            : $this->controller->listRefresh();
    }

}