/*
 * Copyright (c) 2016 Planeta del Este . 
 *
 * app.js is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

+(function ($) {
    var OCFeatures = function () {
    };

    OCFeatures.prototype.init = function () {
        this.control();
    };

    /* ==================================================
     Control initialization
     Find for all elements with parameter data-control
     ================================================== */
    /**
     *
     * @param $el
     * @returns {OCFeatures}
     */
    OCFeatures.prototype.control = function ($el) {
        var self = this;
        if (!$($el).length) {
            $el = $(document);
        }
        $el.find('[data-control]').each(function () {
            var $this = $(this),
                options = $this.data(),
                method = options['control'];

            if (options['controlInitialized']) {
                return false;
            }

            if ($.type(self[method]) == 'function') {
                $(this).data('controlInitialized', true);
                self[method].call(self, $this, options);
            }
        });

        return this;
    };

    OCFeatures.prototype.check = function ($el, options) {
        $el = $($el);
        if (!$el.length)
            return this;
        var toggleClass = 'btn-default btn-success',
            $checkbox = $el.find(':checkbox:not(:disabled):first');

        if(!$checkbox.length)
            return this;

        $checkbox.hide();

        if (options['checkToggleClass']) {
            toggleClass = options.checkToggleClass;
        }

        $el.on('click', function (ev) {
            ev.preventDefault();
            $checkbox.prop('checked', !$checkbox.is(':checked')).trigger('change');
            $el.toggleClass(toggleClass);

        });
    };


    $.ocFeatures = new OCFeatures();

    $(document).ready(function () {
        $.ocFeatures.init();
    });

})(jQuery);