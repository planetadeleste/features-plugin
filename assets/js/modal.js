/**
 * Created by Alvaro on 03/08/14.
 */
+function ($) {
    "use strict";

    var ManageModal = function () {

        this.clickRecord = function (recordId) {
            var newPopup = $('<a />');

            newPopup.popup({
                handler: 'onUpdateForm',
                extraData: {
                    'record_id': recordId,
                }
            })
        };

        this.createRecord = function () {
            var newPopup = $('<a />');
            newPopup.popup({ handler: 'onCreateForm' })
        }

    };

    $.manageModal = new ManageModal;

}(window.jQuery);
